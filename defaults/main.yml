# Copyright 2020-2024 Chris Croome
#
# This file is part of the Webarchitects Icinga Ansible role.
#
# The Webarchitects Icinga Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Icinga Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Icinga Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
icinga_hosts_dir: "/etc/icinga2/zones.d/{{ icinga_master_node }}"
icinga_api_users_dir: /etc/icinga2/conf.d/api-users

# The local_tmp variable appears not to work
# icinga_local_tmp: "{{ local_tmp }}"
icinga_local_tmp: /tmp

icinga_admins:
  - root@localhost

# https://icinga.com/docs/icingaweb2/latest/modules/monitoring/doc/05-Command-Transports/#icinga-2-preparations
icinga_api_users:
  root:
    permissions:
      - "*"
  icingaweb2:
    permissions:
      - "status/query"
      - "actions/*"
      - "objects/modify/*"
      - "objects/query/*"

# Set icinga_auto_generate_nodes_group to true for all hosts with a icinga_parent_node to be automatically added to the icinga_agent_nodes group
icinga_auto_generate_nodes_group: false

# https://icinga.com/docs/icinga2/latest/doc/14-features/
icinga_feature_disabled:
  - command
  - compatlog
  - debuglog
  - elasticsearch
  - gelf
  - graphite
  - influxdb
  - influxdb2
  - livestatus
  - opentsdb
  - perfdata
  - statusdata
icinga_feature_enabled:
  - api
  - checker
  - icingadb
  - ido-mysql
  - mainlog
  - notification
  - syslog

# See the documentation here for plugin check commands:
# https://icinga.com/docs/icinga-2/snapshot/doc/10-icinga-template-library/#plugin-check-commands-for-monitoring-plugins
#
# Default agent checks, used if host specific checks are not defined
icinga_default_agent_check_commands:
  apt:
  disk_root:
    command: disk
    args:
      disk_partitions: /
      disk_wfree: 20%
      disk_cfree: 10%
  dns:
  load:
    args:
      load_percpu: 1
  http:
    args:
      http_port: 443
      http_ssl: 1
      http_sni: 1
      http_uri: /
  ping:
  ssh:
  swap:
  users:
    args:
      users_wgreater: 2
      users_cgreater: 4
# Default remote checks, used if host specific checks are not defined
icinga_default_remote_check_commands:
  ping:
  ssh:

# Set icinga_iptables_check_fail to true only after everything is working!
icinga_iptables_check_fail: false
# IPv4 rule for Icinga
icinga_iptables_ipv4_rule: "-A INPUT -s {{ icinga_master_node_ipv4 }}/32 -p tcp -m state --state NEW -m tcp --dport 5665 -j ACCEPT"
# Insert the Icinga IPv4 rule before this one
icinga_iptables_ipv4_insertbefore: -A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
# IPv4 rule to match against the above query
icinga_iptables_ipv4_check_rule: "state NEW tcp dpt:5665"

icinga_mariadb_socket: /var/run/mysqld/mysqld.sock

icinga_master_node_ipv4: "{{ hostvars[icinga_master_node].ansible_host }}"

# icinga_notifications_mail:
#   - name: Foo Bar
#     email: foo.bar@example.org

icinga_verify: true
...
