# Ansible Icinga Role

See the [Icinga Monitoring Network](https://git.coop/webarch/icinga-network) repo for documentation and the [Icinga Web 2 Role](https://git.coop/webarch/icingaweb).
