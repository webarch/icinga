# Copyright 2020-2024 Chris Croome
#
# This file is part of the Webarchitects Icinga Ansible role.
#
# The Webarchitects Icinga Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Icinga Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Icinga Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
# icingacli is a simple CLI tool for Icingaweb2 and its modules
- name: Check if icingacli is installed
  ansible.builtin.command: which icingacli
  check_mode: false
  changed_when: false
  delegate_to: "{{ icinga_master_node }}"
  register: icinga_which_icingacli
  failed_when: icinga_which_icingacli is not regex('0|1')
  tags:
    - icinga

- name: Icinga agent node connection check tasks
  block:

    # Note this lists all agent and remote nodes
    - name: List all configured Icinga2 agent and remote nodes
      ansible.builtin.command: icingacli monitoring list hosts --format=json
      check_mode: false
      delegate_to: "{{ icinga_master_node }}"
      register: icinga_icingacli_monitoring_list_hosts_command

    - name: Set a hash array of all configured Icinga2 agent and remote nodes
      ansible.builtin.set_fact:
        icinga_icingacli_hosts: "{{ icinga_icingacli_monitoring_list_hosts_command.stdout | from_json }}"
        icinga_hosts_existing: []

    - name: Debug Icinga2 agent and remote node configured hash array
      ansible.builtin.debug:
        var: icinga_icingacli_hosts
        verbosity: 2

    # Note this removes remote nodes from the list
    - name: Set an array of all configured Icinga agent nodes for Ansible < 2.13
      ansible.builtin.set_fact:
        icinga_hosts_existing: "{{ icinga_hosts | default([]) }} + [ '{{ host.host_name }}' ]"
      when:
        - ansible_version.full is version('2.13.0', '<')
        - host.host_name not in groups['icinga_remote_nodes']
      loop: "{{ icinga_icingacli_hosts }}"
      loop_control:
        loop_var: host
        label: "{{ host.host_name }}"

    # Note this removes remote nodes from the list
    - name: Set an array of all configured Icinga agent nodes for Ansible >= 2.13
      ansible.builtin.set_fact:
        icinga_hosts_existing: "{{ icinga_hosts | default([]) + [host.host_name] }}"
      when:
        - ansible_version.full is version('2.13.0', '>=')
        - host.host_name not in groups['icinga_remote_nodes']
      loop: "{{ icinga_icingacli_hosts }}"
      loop_control:
        loop_var: host
        label: "{{ host.host_name }}"

    - name: Debug Icinga2 agent nodes configured array
      ansible.builtin.debug:
        var: icinga_hosts_existing
        verbosity: 1

    - name: Set an array of all agent nodes that should be configured
      ansible.builtin.set_fact:
        icinga_hosts_proposed: "{{ groups['icinga_agent_nodes'] | default([]) }}"
      when: groups['icinga_agent_nodes'] is defined

    - name: Debug Icinga2 agent nodes proposed array
      ansible.builtin.debug:
        var: icinga_hosts_proposed
        verbosity: 1

    - name: Set an array of not configured agent nodes
      ansible.builtin.set_fact:
        icinga_hosts_missing: "{{ icinga_hosts_proposed | difference(icinga_hosts_existing) }}"

    - name: Print Icinga agent nodes that need to be configured
      ansible.builtin.debug:
        var: icinga_hosts_missing
      when:
        - icinga_hosts_missing is defined
        - icinga_hosts_missing | length > 0

  when:
    - icinga_which_icingacli.rc == 0
    - groups['icinga_master_nodes'] is defined
    - groups['icinga_agent_nodes'] is defined
    - (inventory_hostname in groups['icinga_master_nodes']) or (inventory_hostname in groups['icinga_agent_nodes'])
  tags:
    - icinga
...
