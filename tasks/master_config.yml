# Copyright 2020-2024 Chris Croome
#
# This file is part of the Webarchitects Icinga Ansible role.
#
# The Webarchitects Icinga Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Icinga Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects Icinga Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
- name: Icinga master configuration
  block:

    - name: Salt present in /root/icinga.salt
      ansible.builtin.shell: pwgen -n 32 1 > /root/icinga.salt
      args:
        creates: /root/icinga.salt

    - name: Slurp /root/icinga.salt
      ansible.builtin.slurp:
        src: /root/icinga.salt
      register: icinga_salt_b64encoded

    - name: Decode the base64 encoded version of salt and set a variable
      ansible.builtin.set_fact:
        icinga_salt: "{{ icinga_salt_b64encoded['content'] | b64decode | trim }}"

    - name: Icinga master configuration files in place
      ansible.builtin.template:
        src: "{{ icinga_conf }}.j2"
        dest: "/etc/icinga2/{{ icinga_conf }}"
        owner: nagios
        group: nagios
        mode: "0640"
      no_log: "{% if ansible_check_mode | bool %}false{% else %}true{% endif %}"
      loop:
        - checks.conf
        - constants.conf
        - icinga2.conf
        - zones.conf
      loop_control:
        loop_var: icinga_conf

    - name: Icinga conf.d configuration files in place
      ansible.builtin.template:
        src: "{{ icinga_conf }}.j2"
        dest: "/etc/icinga2/conf.d/{{ icinga_conf }}"
        owner: nagios
        group: nagios
        mode: "0640"
      loop:
        - services.conf
        - users.conf
      loop_control:
        loop_var: icinga_conf

  when:
    - icinga_master_node is defined
    - inventory_hostname == icinga_master_node
    - groups['icinga_master_nodes'] is defined
    - inventory_hostname in groups['icinga_master_nodes']
  tags:
    - icinga
...
